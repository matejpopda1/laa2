# LAA2

Vstupem je soubor ve kterém je  matice která by měla mít tvar čísel oddělených mezerami, například:


1 1 3 <br />
9 4 9 <br />

nebo

3.78 7.12 7.33 4.50 <br />
8.73 8.19 1.53 1.45 <br />
1.86 1.76 6.21 6.95 <br />
6.84 7.67 4.17 5.14 <br />

Pro Cramerovo pravidlo matice musí mít tvar n x n+1 <br />
Pro inverzní matice musí mít tvar n x n

Argumentem u --option se mění který úkol má běžet, tento argument je povinný, možnosti jsou:<br />
* inverse - výpočet inverzní matice za pomoci gausovy eliminace
* cramer - výpočet řešení soustavy algebraických rovnic za pomoci Cramerova pravidla
* adjung - výpočet inverzní matice za pomoci adjungované matice

Argumentem u --input se volí vstupní soubor
