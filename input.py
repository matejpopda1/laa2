import argparse


# převedení souboru do matice
def file_to_matrix(input_file):
    x = []
    column_length = -1  # inicializace počtu sloupců
    file = open(input_file, "r")
    for i in file:  # parsing input into a list
        temp = i.strip()
        temp = temp.rsplit()
        for p in range(temp.count("")):  # odstranění whitespace znaků
            temp.remove("")
        if temp:  # zjišťování počtu sloupců v každém řádku
            if column_length == -1:
                column_length = len(temp)
            elif column_length != len(temp):
                print("Matice je špatně zadaná - v řádcích je různý počet sloupců")
                exit()
            for j in range(len(temp)):  # kontrola vstupu
                test = temp[j].replace("-", "").replace(".", "")  # tímto dovolíme vstup záporných a desetinných čísel
                if not test.isnumeric():
                    print("Matice je špatně zadaná - nejsou v ní pouze čísla")
                    exit()
            for number in range(len(temp)): # přetypování všech čísel ze str na float
                temp[number] = float(temp[number])

            x.append(temp)

    file.close()
    return x


def arguments():  # parser pro argumenty z příkazové řádky
    parser = argparse.ArgumentParser(description='Programovací úlohy z LAA2.')
    parser.add_argument('--option', "-o", type=str, nargs='?',
                        help='Povinný argument, říká co se má provést s vloženou maticí, možnosti jsou inverse (výpočet inverzní matice za pomoci gausovy eliminace), cramer (výpočet řešení soustavy algebraických rovnic za pomoci Cramerova pravidla) nebo adjung (výpočet inverzní matice za pomoci adjungované matice)')
    parser.add_argument('--input', "-i", type=str, nargs='?',
                        help='Cesta ke vstupnímu souboru, výchozí je \"input-file.txt\" v adresáři tohoto programu',
                        default="input-file.txt")
    args = parser.parse_args()
    return args
