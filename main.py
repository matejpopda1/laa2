from matrix import Matrix
from input import file_to_matrix, arguments


if __name__ == "__main__":
    args = arguments().option  # výstup z -option
    input_file = arguments().input  # výstup z -input

    print("Vstupní matice může obsahovat čísla s desetinou čárkou, musí být čtvercová pro výpočet inverzní matice, nebo mít n řádků a n+1 sloupců pro cramerovo pravidlo")
    matice = Matrix(file_to_matrix(input_file))
    print("Vstup je: \n")
    matice.print()
    if args == "inverse":  # výpočet inverzní matice za pomoci gausovy eliminace
        print("Inverzní matice má tvar: \n")
        matice.inverse()
        matice.print()
    elif args == "cramer":  # výpočet řešení soustavy algebraických rovnic za pomoci Cramerova pravidla
        print("Výsledkem Cramerova pravidla je: \n")
        x = ""
        for each in matice.cramer():  # hezčí výstup
            x += " "
            x += str(each)
        print(x)
    elif args == "adjung":  # výpočet inverzní matice za pomoci adjungované matice
        print("Inverzní matice má tvar: \n")
        matice.inverse_through_adjunct()
    else:
        print("Chyba - špatně zvolený argument u --option nebo není zvolený vůbec")
