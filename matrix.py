import copy


class Matrix:
    def __init__(self, input_matrix: list):
        self.number_of_lines = len(input_matrix)  # velikost matice
        self.number_of_columns = len(input_matrix[0])
        self.matrix = input_matrix
        self.determinant_changes_through_gaussian_elimination = 1

    # ekvivalentni řádkové úpravy:
    def __multiply_line(self, line_number, x):  # násobení řádky
        for i in range(self.number_of_columns):
            self.matrix[line_number][i] = self.matrix[line_number][i] * x
        self.determinant_changes_through_gaussian_elimination = x * self.determinant_changes_through_gaussian_elimination

    def __swap_lines(self, line1, line2):  # prohození řádek
        for i in range(self.number_of_columns):
            temp = self.matrix[line1][i]
            self.matrix[line1][i] = self.matrix[line2][i]
            self.matrix[line2][i] = temp
        self.determinant_changes_through_gaussian_elimination = -self.determinant_changes_through_gaussian_elimination

    def __add_lines(self, line1, line2):  # přičtení řádek - přičítáme k prvnimu argumentu funkce
        for i in range(self.number_of_columns):
            self.matrix[line1][i] += self.matrix[line2][i]

    def __subtract_lines(self, line1, line2):  # odečtení řádků - odečítáme od prvnimu argumentu funkce
        for i in range(self.number_of_columns):
            self.matrix[line1][i] -= self.matrix[line2][i]

    # vypis matici
    def print(self):
        output = ""
        for i in range(self.number_of_lines):
            for j in range(self.number_of_columns):
                output = output + str(self.matrix[i][j]) + " "
            output = output + "\n"
        print(output)

    def htt(self):  # převedení matice do horního trojúhelníkového tvaru
        for pivot in range(self.number_of_lines - 1):
            for line in range(pivot + 1, self.number_of_lines):
                if self.matrix[line][pivot] and self.matrix[pivot][pivot]:
                    factor = self.matrix[pivot][pivot]/self.matrix[line][pivot]
                    self.__multiply_line(line, factor)
                    self.__subtract_lines(line, pivot)
                elif self.matrix[pivot][pivot] == 0:  # prohození nulového a nenulového řádky pro zabránění dělení nulou
                    for i in range(line, self.number_of_lines):
                        if self.matrix[i][pivot]:
                            self.__swap_lines(pivot, i)
                            break
                    if self.matrix[line][pivot]:
                        factor = self.matrix[pivot][pivot] / self.matrix[line][pivot]
                        self.__multiply_line(line, factor)
                        self.__subtract_lines(line, pivot)

    # ziskani 1 na diagonále
    def __diag(self):
        for i in range(self.number_of_lines):
            if self.matrix[i][i]:  # pokud je matice regularni tak je toto if zbytecne
                self.__multiply_line(i, 1/self.matrix[i][i])

    # zpetny chod (předpoklad 1 na diagonale)
    def __backwards(self):
        for i in range(self.number_of_lines-1, 0, -1):
            for j in range(i-1, -1, -1):
                temp = self.matrix[j][i]
                if temp:
                    self.__multiply_line(i, temp)
                    self.__subtract_lines(j, i)
                    self.__multiply_line(i, 1/temp)
    
    # nalezení inverzní matice za pomoci uplné gausovy eliminace
    def inverse(self):
        if not self.__is_square():
            print("Matice je špatně zadaná - není čtvercová")
            exit()
        self.__add_identity()  # rozšíříme matice o jednotkovou matice
        self.htt()  # převedeme matici do horního stupňovitého tvaru
        self.__diag()  # diagonalu vydelime tak abychom na ni dostaly 1
        if not self.__is_regular():  # zkontrolujeme regularitu matice
            print("Chyba - Neexistuje inverzní matice, vstupní matice není regulární")
            exit()
        self.__backwards()  # provedeme zpetny chod
        self.__remove_left_side()  # odstranime vzniklou jednotkovou matici na leve strane

    def __add_identity(self):  # přidání jednotkové matice na pravou stranu
        for line in range(self.number_of_lines):
            for i in range(self.number_of_lines):
                if line == i:
                    self.matrix[line].append(1)
                else:
                    self.matrix[line].append(0)
        self.number_of_columns = self.number_of_columns * 2

    def __is_regular(self):  # zkoumání zda je matice regulární, předpokladem je že je v horním stupňovitém tvaru
        for i in range(self.number_of_lines):
            if self.matrix[i][i] == 0:  # an error can come up because of an rounding error
                return False
        return True

    def __remove_left_side(self):  # odstranění vzniklé jednotkové matice při hledání inverzní matice
        for line in range(self.number_of_lines):
            for i in range(self.number_of_lines):
                self.matrix[line].pop(0)
        self.number_of_columns = int(self.number_of_columns/2)

    def __is_square(self):  # kontrola zda je matice čtvercová
        if self.number_of_columns == self.number_of_lines:
            return True
        else:
            return False

    def _is_system_of_equations(self):  # kontrola zda je matice soustava algebraických rovnic tzn ma n radku a n+1 sloupcu
        if self.number_of_columns == self.number_of_lines + 1:
            return True
        else:
            return False

    def determinant(self):  # výpočet determinantu
        self.htt()  # horni trojuhelnikovy tvar
        x = 1
        for i in range(self.number_of_lines):
            x = x * self.matrix[i][i]
        x = x / self.determinant_changes_through_gaussian_elimination
        return x

    def cramer(self):  # Cramerovo pravidlo
        if not self._is_system_of_equations():  # kontrola rozměrů matice
            print("Matice je špatně zadaná - nemá rozměry n na n+1")
            exit()
        x = []
        for i in range(self.number_of_lines):  # odstranení pravého sloupce
            x.append(self.matrix[i].pop(self.number_of_columns-1))
        self.number_of_columns -= 1

        solution = []
        original_matrix = copy.deepcopy(self.matrix)
        determinant = self.determinant()  # determinant matice
        if (not determinant) or abs(determinant) < 1e-12:  # druhá podmínka se snaží odstranit alespoň některé chyby vzniklé numerickými nestabilitami
            print("Chyba - matice není regulární - řešení soustavy neexistuje")
            exit()
        for i in range(self.number_of_columns):  # zaměna sloupců za pravý sloupec
            self.matrix = copy.deepcopy(original_matrix)
            self.determinant_changes_through_gaussian_elimination = 1
            for j in range(self.number_of_lines):
                self.matrix[j][i] = x[j]
            solution.append(self.determinant() / determinant)

        return solution

    def inverse_through_adjunct(self):  # nalezení inverzní matice pomocí matice adjungované
        if not self.__is_square():
            print("Matice je špatně zadaná - není čtvercová")
            exit()
        adjunct = self.adjunct()
        determinant = self.determinant()
        if not determinant:
            print("Chyba - Neexistuje inverzní matice, vstupní matice není regulární")
            exit()
        for i in range(self.number_of_lines):
            for j in range(self.number_of_columns):
                self.matrix[i][j] = adjunct[i][j]/determinant
        self.print()

    def adjunct(self):  # vytvoření adjungované matice
        adjunct = []
        for i in range(self.number_of_lines):
            x = []
            for j in range(self.number_of_columns):
                x.append(self.cofactor(j, i))
            adjunct.append(x)
        return adjunct

    def cofactor(self, line, column):  # nalezení algebraického doplňku
        matrix = []
        for lines in range(self.number_of_lines):  # vyškrtnutí řádků a sloupců
            current_column = []
            if not (lines == line):
                for columns in range(self.number_of_columns):
                    if not (column == columns):
                        current_column.append(self.matrix[lines][columns])
                matrix.append(current_column)
        submatrix = Matrix(matrix)
        return pow(-1, line+column) * submatrix.determinant()
